<?php
/*
Plugin Name: Revisal
Description: Requires livedrafts
Author: Tim Shultis
Author URL: http://morsekode.com
Version: 0.1
*/


//Banking off some functionality included in another plugin.
//The folder/plugin is included along with the repo.
//You would need to install that.
if ( class_exists( 'liveDrafts' ) ) {

	/*
		extending LiveDrafts
	 */
	class Revisal extends liveDrafts{

		const POST_STATUS = "pending";
		const POST_PARENT_META = "_pc_liveId";

		public function __construct() {
			parent::__construct();

			//Thus adds a custom post_status.  We are using pending a default status, so disabled for now.
			//add_action( 'init', array( $this, 'revisal_custom_post_status') );

			add_action( 'admin_footer-post.php', array( $this, 'revisal_append_post_status_list' ) );
			add_action( 'admin_head-post.php', array( $this, 'hide_buttons_on_revisal' ) );

			//When viewing the post list in admin, add the small tag that indicates the post_status
			add_filter( 'display_post_states', array( $this, 'revisal_display_state' ) );

			//add compare button to wp-admin publish block
			add_action( 'post_submitbox_misc_actions', array( $this, 'compare_post_action_button' ) );

			//add pop-up content to bottom of page (hidden). Fired inside lightbox
			add_action( 'admin_footer',  array( $this, 'add_compare_revisal_content' ) );
			add_action( 'admin_notices', array( $this, 'revisal_exists_warning' ) );
		}

		/*
	    * User warning to indicate that there is a more recent verision of this post in existence
	     */
		public function revisal_exists_warning() {
			global $post, $wpdb, $pagenow;

			//if not in admin screens then stop running
			if ( !is_admin() )
				return;

			if ( $pagenow != 'post.php' )
				return;

			//if not a post status of publish then return
			if ( $post->post_status != 'publish' )
				return;

			$pending_posts = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE meta_key = '_pc_liveId' && meta_value = $post->ID" );

			if ( !$pending_posts && empty( $pending_posts ) )
				return;

			if ( count( $pending_posts ) > 1 ) {
				echo '<div class="postbox" style="padding:15px;"><strong>Multiple post revisions for this post exist</strong>';
			}

			foreach ( $pending_posts as $pending_post ) {
				echo '<div id="message" class="updated below-h2" style="margin:0px;margin-top:5px;"><p>A more updated Draft of this post exists already, please consider editing it instead.<a href ="'.get_edit_post_link( $pending_post->post_id ).'">Edit</a></p></div>';
			}

			if ( count( $pending_posts ) > 1 ) {
				echo '</div>';
			}
		}

		//Get custom post types for this theme.  We use this, so we can enable the revisal process on all the post types this function provides.
		//aka... you may have more than just regular ol' posts
		private function get_custom_post_types() {
			$names;
			$args = array(
				'public'   => true,
				'_builtin' => false
			);

			$output = 'names'; // names or objects, note names is the default
			$operator = 'and'; // 'and' or 'or'

			$post_types = get_post_types( $args, $output, $operator );

			foreach ( $post_types  as $post_type ) {
				$names[] = $post_type;
			}
			return $names;
		}

		/*
	    *The button to open a GIT style way of view a side-by-side change between two posts
	    */
		public function compare_post_action_button() {
			global $post;

			//if this is not a revisal post, then dont show the button
			if ( $post->post_status != self::POST_STATUS )
				return;

			//check to make sure this post has a parent post to compare too... or dont show the button
			$_pc_liveId = get_post_meta( $post->ID, self::POST_PARENT_META, true );
			if ( !$_pc_liveId )
				return;


			//our popup's title
			$title = 'Compare';

			//append the html
			$context .= "<div class='misc-pub-section misc-pub-section-last'>".$title.": <span><a href='#TB_inline?width=500&height=600&inlineId=popup_container_revisal' class='thickbox' title='".$title."'>
			Show</a></span></div>";

			echo $context;
		}

		/*
		* a GIT style way of view a side-by-side change between two posts
		*/
		public function add_compare_revisal_content() {

			global $post;
			if ( $post->post_status != self::POST_STATUS )
				return;

			$no_diff_message = "<h2>Nothing To Show</h2><p>The revised post matches the live post.  There are no differences to show.</p>";


			//get the current posts content
			$cur_post = get_post( $post->ID, OBJECT, 'edit' );
			$cur_title = $cur_post->post_title;
			$cur_content = $cur_post->post_content;
			$cur_excerpt = $cur_post->post_excerpt;
			$cur_taxes;
			$cur_acf_meta;

			//get the parent post (live post) content ( obtained from the post meta '_pc_liveId')
			$_pc_liveId = get_post_meta( $post->ID, self::POST_PARENT_META, true );
			$lv_post = get_post( $_pc_liveId, OBJECT, 'edit' );
			$lv_title = $lv_post->post_title;
			$lv_content = $lv_post->post_content;
			$lv_excerpt = $lv_post->post_excerpt;
			$lv_taxes;
			$lv_acf_meta;

			//get custom taxonomies
			$args = array(
				'public'   => true,
				'_builtin' => false
			);
			$output = 'names'; // or objects
			$operator = 'and'; // 'and' or 'or'
			$taxonomies = get_taxonomies( $args, $output, $operator );
			if ( $taxonomies ) {
				foreach ( $taxonomies  as $taxonomy ) {

					$term_list_lv = wp_get_post_terms( $_pc_liveId, $taxonomy, array( "fields" => "names" ) );
					$term_list_cur = wp_get_post_terms( $post->ID, $taxonomy, array( "fields" => "names" ) );

					if ( !empty( $term_list_lv ) ) {
						$lv_taxes[] =  $term_list_lv;
					}
					if ( !empty( $term_list_cur ) ) {
						$cur_taxes[] =  $term_list_cur;

					}
				}
			}

			// Custom meta data for revisal post
			$custom = get_post_custom( $cur_post->ID );
			foreach ( $custom as $ckey => $cvalue ) {
				if ( $ckey != '_edit_lock' && $ckey != '_edit_last' && $ckey!= 'editor_notes' ) {
					foreach ( $cvalue as $mvalue ) {
						//checking for acf existence, so we can output accordinley.
						if ( class_exists( 'Acf' ) ) {
							//ignoring acf fields that contain an _ (they dont contain user displayed data)
							if ( $ckey[0]!= "_" ) {
								$acf_object = get_field_object( $ckey, $cur_post->ID );

								if ( $acf_object['type'] == 'image' ) {
									//if( wp_attachment_is_image( get_field($ckey, $cur_post->ID) ){
									$acf_image_url = wp_get_attachment_image_src( get_field( $ckey, $cur_post->ID ) );
									$cur_acf_meta[] = array(
										'label' => $acf_object['label'],
										'value' => $acf_image_url[0],
										'image' => '<img src="'.$acf_image_url[0].'" />'
									);
								} else {
									$cur_acf_meta[] = array(
										'label' => $acf_object['label'],
										'value' => get_field( $ckey, $cur_post->ID )
									);
								}
							}
						}
					}
				}
			}

			// Get Custom meta data for Live post
			$custom = get_post_custom( $lv_post->ID );
			foreach ( $custom as $ckey => $cvalue ) {
				if ( $ckey != '_edit_lock' && $ckey != '_edit_last' && $ckey!= 'editor_notes' ) {
					foreach ( $cvalue as $mvalue ) {

						//checking for acf existence, so we can output accordinley.
						if ( class_exists( 'Acf' ) ) {
							//ignoring acf fields that contain an _ (they dont contain user displayed data)
							if ( $ckey[0]!= "_" ) {
								$acf_object = get_field_object( $ckey, $lv_post->ID );
								if ( $acf_object['type'] == 'image' ) {
									$acf_image_url = wp_get_attachment_image_src( get_field( $ckey, $lv_post->ID ) );
									$lv_acf_meta[] = array(
										'label' => $acf_object['label'],
										'value' => $acf_image_url[0],
										'image' => '<img src="'.$acf_image_url[0].'" />'
									);
								} else {
									$lv_acf_meta[] = array(
										'label' => $acf_object['label'],
										'value' => get_field( $ckey, $lv_post->ID )
									);
								}

							}
						}
					}
				}
			}

			$left = new stdClass;
			$left->title = $lv_title;
			$left->content = $lv_content;
			$left->excerpt = $lv_excerpt;
			if ( !empty( $lv_taxes ) ) {
				for ( $i=0; $i<count( $lv_taxes ); $i++ ) {
					$left->taxes .=implode( ",", $lv_taxes[$i] )." ";
				}
			} else {
				$left->taxes = '';
			}


			$left->acf_meta = $lv_acf_meta;

			$right = new stdClass;
			$right->title = $cur_title;
			$right->content = $cur_content;
			$right->excerpt = $cur_excerpt;
			if ( !empty( $cur_taxes ) ) {
				for ( $i=0; $i<count( $cur_taxes ); $i++ ) {
					$right->taxes .=implode( ",", $cur_taxes[$i] )." ";
				}
			} else {
				$right->taxes = '';
			}

			$right->acf_meta = $cur_acf_meta;

			$args_title = array(
				'title'           => 'Title',
				'title_left'      => 'Live Version',
				'title_right'     => 'Suggested Revision'
			);

			$args_content = array(
				'title'           => 'Content',
				'title_left'      => 'Live Version',
				'title_right'     => 'Suggested Revision'
			);

			$args_excerpt = array(
				'title'           => 'Excerpt',
				'title_left'      => 'Live Version',
				'title_right'     => 'Suggested Revision'
			);

			$args_taxes = array(
				'title'           => 'Taxonomy',
				'title_left'      => 'Live Version',
				'title_right'     => 'Suggested Revision'
			);

			//build text diffs
			$compare_table = wp_text_diff( $left->title, $right->title, $args_title );
			$compare_table .= wp_text_diff( $left->content, $right->content, $args_content );
			$compare_table .= wp_text_diff( $left->excerpt, $right->excerpt, $args_excerpt );
			$compare_table .= wp_text_diff( $left->taxes, $right->taxes, $args_taxes );
			for ( $i=0; $i<count( $left->acf_meta ); $i++ ) {
				$args_meta = array(
					'title'           => '<p style="color:red">'.$left->acf_meta[$i]['label'].'</p>',
					'title_left'      => 'Live Version',
					'title_right'     => 'Suggested Revision'
				);
				$compare_table .= wp_text_diff( $left->acf_meta[$i]['value'], $right->acf_meta[$i]['value'], $args_meta );
				if ( $left->acf_meta[$i]['image'] ) {
					$compare_table .= '<table class="diff"><tr><td>'.$left->acf_meta[$i]['image'].'</td>';
				}
				if ( $right->acf_meta[$i]['image'] ) {
					$compare_table .= '<td>'.$right->acf_meta[$i]['image'].'</td></tr></table>';
				}
			}

			//pop-up window content
			echo '<div id="popup_container_revisal" style="display:none;">';
			echo $compare_table != "" ? $compare_table : $no_diff_message;
			//echo '<a href="'.$cur_post->guid.'">Preview Revised Post</a>';
			echo '</div>';
		}

		//Setting up custom post status Revisal
		public function revisal_custom_post_status() {
			register_post_status( self::POST_STATUS, array(
					'label'                     => _x( ucfirst( self::POST_STATUS ), 'post' ),
					'public'                    => true,
					'exclude_from_search'       => false,
					'show_in_admin_all_list'    => true,
					'show_in_admin_status_list' => true,
					'label_count'               => _n_noop( ucfirst( self::POST_STATUS ).'s <span class="count">(%s)</span>', ucfirst( self::POST_STATUS ).'s <span class="count">(%s)</span>' ),
				) );
		}

		//Allow for Revisal to be chosen from the admin view. (much like you can pick draft, publish, etc);
		public function revisal_append_post_status_list() {
			global $post;
			$complete = '';
			$label = '';
			if ( in_array( $post->post_type, $this->get_custom_post_types() ) ) {
				if ( $post->post_status == self::POST_STATUS ) {
					$complete = ' selected=\"selected\"';
					$label = '<span id=\"post-status-display\"> '.ucfirst( self::POST_STATUS ).'</span>';
				}
				echo '
		          <script>
		          jQuery(document).ready(function($){
		               $("select#post_status").append("<option value=\"'.self::POST_STATUS.'\" '.$complete.'>'.ucfirst( self::POST_STATUS ).'</option>");
		               $(".misc-pub-section label").append("'.$label.'");
		          });
		          </script>
		          ';
			}
		}



		//IF ON A REVISAL EDIT POST SCREEN, WE MAKE SOME CHANGES
		//This is a bit ugly
		public function hide_buttons_on_revisal() {
			global $post, $current_user;
			//hide the create revisal button if on a revisal
			//hide the the drop down that assigns post status.
			if ( in_array( $post->post_type, $this->get_custom_post_types() ) && $post->post_status == self::POST_STATUS ) {

				if ( in_array( "community_editor", $current_user->roles ) ) {
				?>
					<script type="text/javascript" >

						jQuery(document).ready(function() {

							jQuery('#save-action').css('display', 'none');
							jQuery('.misc-pub-section:first .edit-post-status').css('display', 'none');
							jQuery('.misc-pub-section:first #post-status-display').text('Revisal');
						});

					</script>

				<?php
					// SPECIFICALLY ALLOW ADMINS TO SAVE DRAFTS OF THE PENDING POSTS
				} else {
				?>
					<script type="text/javascript" >

						jQuery(document).ready(function() {

							//jQuery('#save-action').css('display', 'none');
							jQuery('.misc-pub-section:first .edit-post-status').css('display', 'none');
							jQuery('.misc-pub-section:first #post-status-display').text('Revisal');
						});

					</script>

				<?php
				}
			}
		}

		/*
		Funtion override from parent class
		 */
		function adminHead() {
			global $post;

			global $current_user;
			get_currentuserinfo();

			// Only show the revisal button on published pages
			// the submit value "Submit for Review" is important here.  It is checked on post (function prePostUpdate)
			if ( in_array( $post->post_type, $this->get_custom_post_types() ) && $post->post_status == 'publish' ) {

				if ( in_array( "administrator", $current_user->roles ) ) {
				?>
					<script type="text/javascript" >

						// Add save revisal button to published pages so admin could use it, if they want
						jQuery(document).ready(function() {

							jQuery('<input type="submit" class="button button-highlighted" tabindex="4" value="Submit for Review" id="save-post" name="save">').prependTo('#save-action');

						});

					</script>
					<?php
				} else if ( in_array( "community_editor", $current_user->roles ) ) {
					?>
					<script type="text/javascript" >
						// Add save revisal button to published pages and remove the normal publish buton
						jQuery(document).ready(function() {
							jQuery('#publishing-action #publish').remove();
							jQuery('<input type="submit" class="button button-primary button-large" tabindex="4" value="Submit for Review" id="save-post" name="save">').prependTo('#publishing-action');
							//hide some other options that users wont need
							jQuery('.misc-pub-section:first .edit-post-status').css('display', 'none');
							jQuery('#visibility').css('display', 'none');
						});

					</script>
					<?php
					}
			}
		}

		//Doing the work here to duplicate a post as a revision
		public function prePostUpdate( $id ) {
			// Check if this is an auto save routine. If it is we dont want to do anything
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
				return $id;

			// Only continue if this request is for our custom post types
			if ( !in_array( $_POST['post_type'], $this->get_custom_post_types() ) ) {
				return $id;
			}

			// Check permissions
			//if (!current_user_can('edit_' . ($_POST['post_type'] == 'posts' ? 'posts' : 'page'), $id )) {
			// return $id;
			//}

			// Catch only when a draft is saved of a live page
			if ( $_REQUEST['save'] == 'Submit for Review' && $_REQUEST['post_status'] == 'publish' ) {

				// Duplicate post and set as a pending (aka SELF::POST_STATUS)
				$draftPost = array(
					'menu_order' => $_REQUEST['menu_order'],
					'comment_status' => ( $_REQUEST['comment_status'] == 'open' ? 'open' : 'closed' ),
					'ping_status' => ( $_REQUEST['ping_status'] == 'open' ? 'open' : 'closed' ),
					'post_author' => $_REQUEST['post_author'],
					'post_category' => ( isset( $_REQUEST['post_category'] ) ? $_REQUEST['post_category'] : array() ),
					'post_content' => $_REQUEST['content'],
					'post_excerpt' => $_REQUEST['excerpt'],
					'post_parent' => $_REQUEST['parent_id'],
					'post_password' => $_REQUEST['post_password'],
					'post_status' => self::POST_STATUS,
					'post_title' => $_REQUEST['post_title'],
					'post_type' => $_REQUEST['post_type'],
					'tags_input' => ( isset( $_REQUEST['tax_input']['post_tag'] ) ? $_REQUEST['tax_input']['post_tag'] : '' )
				);

				// Insert the post into the database
				$newId = wp_insert_post( $draftPost );

				//get custom taxonomies and save them
				$args = array(
					'public'   => true,
					'_builtin' => false
				);

				$output = 'names'; // or objects
				$operator = 'and'; // 'and' or 'or'
				$taxonomies = get_taxonomies( $args, $output, $operator );
				if ( $taxonomies ) {
					foreach ( $taxonomies  as $taxonomy ) {
						//$term_list = wp_get_post_terms($id, $taxonomy, array("fields" => "all"));
						$term_list = $_REQUEST['tax_input'][$taxonomy];
						if ( !empty( $term_list ) ) {
							//print_r($term_list);
							wp_set_post_terms( $newId, $term_list, $taxonomy );
						}
					}
				}

				// Custom meta data
				$custom = get_post_custom( $id );
				foreach ( $custom as $ckey => $cvalue ) {
					if ( $ckey != '_edit_lock' && $ckey != '_edit_last' ) {
						foreach ( $cvalue as $mvalue ) {
							add_post_meta( $newId, $ckey, $mvalue, true );
						}
					}
				}

				// Add a hidden meta data value to indicate that this is a draft of a live page
				update_post_meta( $newId, self::POST_PARENT_META, $id );

				//execute action hook.
				do_action( 'save_revisal', $newId, get_post( $newId ) );

				// Send user to new edit page
				wp_redirect( admin_url( 'post.php?action=edit&post=' . $newId ) );
				exit();
			} else if ( $_REQUEST['publish'] == 'Submit for Review' ) {
					do_action( 'save_review', $id, get_post( $id ) );
				}

			// Catch draft pages that need to replace a live page
			if ( isset( $_REQUEST['publish'] ) && current_user_can( 'publish_post' ) ) {

				// Check for post meta that identifies this as a 'live draft'
				$_pc_liveId = get_post_meta( $id, self::POST_PARENT_META, true );

				// If post meta exists then replace live page
				if ( $_pc_liveId != false ) {

					// Duplicate post and set as a draft
					$updatedPost = array(
						'ID' => $_pc_liveId,
						'menu_order' => $_REQUEST['menu_order'],
						'comment_status' => ( $_REQUEST['comment_status'] == 'open' ? 'open' : 'closed' ),
						'ping_status' => ( $_REQUEST['ping_status'] == 'open' ? 'open' : 'closed' ),
						'post_author' => $_REQUEST['post_author'],
						'post_category' => ( isset( $_REQUEST['post_category'] ) ? $_REQUEST['post_category'] : array() ),
						'post_content' => $_REQUEST['content'],
						'post_excerpt' => $_REQUEST['excerpt'],
						'post_parent' => $_REQUEST['parent_id'],
						'post_password' => $_REQUEST['post_password'],
						'post_status' => 'publish',
						'post_title' => $_REQUEST['post_title'],
						'post_type' => $_REQUEST['post_type'],
						'tags_input' => ( isset( $_REQUEST['tax_input']['post_tag'] ) ? $_REQUEST['tax_input']['post_tag'] : '' )
					);


					// Insert the post into the database
					wp_update_post( $updatedPost );

					//get custom taxonomies and save them
					$args = array(
						'public'   => true,
						'_builtin' => false

					);
					$output = 'names'; // or objects
					$operator = 'and'; // 'and' or 'or'
					$taxonomies = get_taxonomies( $args, $output, $operator );
					if ( $taxonomies ) {
						foreach ( $taxonomies  as $taxonomy ) {
							$term_list = wp_get_post_terms( $id, $taxonomy, array( "fields" => "ids" ) );

							if ( !empty( $term_list ) ) {
								//print_r($term_list);
								//foreach ($term_list as $term) {
								wp_set_post_terms( $_pc_liveId, $term_list, $taxonomy );
								//}

							}
						}
					}

					// Clear existing meta data
					$existing = get_post_custom( $_pc_liveId );
					foreach ( $existing as $ekey => $evalue ) {
						delete_post_meta( $_pc_liveId, $ekey );
					}

					// New custom meta data - from draft
					$custom = get_post_custom( $id );
					foreach ( $custom as $ckey => $cvalue ) {
						if ( $ckey != '_edit_lock' && $ckey != '_edit_last' && $ckey != '_pc_liveId' ) {
							foreach ( $cvalue as $mvalue ) {
								add_post_meta( $_pc_liveId, $ckey, $mvalue, true );
							}
						}
					}

					// Delete draft post, force delete since 2.9, no sending to trash
					wp_delete_post( $id, true );

					// Send user to live edit page
					wp_redirect( admin_url( 'post.php?action=edit&post=' . $_pc_liveId ) );
					exit();

				}
			}
		}

		//Add the small text next to the post in the admin list view (Just how 'Draft' shows up to help define the status of the post)
		public function revisal_display_state( $states ) {
			global $post;
			$arg = get_query_var( 'post_status' );
			if ( $arg != self::POST_STATUS ) {
				if ( $post->post_status == self::POST_STATUS ) {
					return array( ucfirst( self::POST_STATUS ) );
				}
			}
			return $states;
		}
	}

	function revisal_init() {
		require_once 'Revisal_Workflow.php';
		global $revisal;
		$revisal = new Revisal();
		$revisal_workflow = new Revisal_Workflow();
	}

	add_action ( 'admin_init', 'revisal_init' );
}
