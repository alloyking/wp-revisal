# WP Revisal #

Warning : This is beta and only configured to work on a specific site for a specific client.  Some settings in revisal-workflow.php would need to be updated as it checks for custom roles and user groups


### So what is this repository for? ###

* For creating revisions of posts that are already 'published' (Wordpress does not natively do this!!).  These revisions need to be admin approved before they replace the live content.

### dependencies ###
Uses another plugin called livedrafts (included in this repo).