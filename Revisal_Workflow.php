<?php
/*
 * controls settings and email process for the plugin
 */

class Revisal_Workflow {

	//left over from testing.  This constant is only used if the site option we add in (add_site_email()) is not populated
	const EMAIL_TO = 'SOME@ADMINUSER';
	

	public function __construct(){
		//custom filters from Revisal class - catching new posts vs revised posts
		add_action( 'save_revisal', array($this, 'wp_revisal_save' ), 10, 2);
		add_action( 'save_review', array($this, 'wp_revisal_save' ), 10, 2);

		//add site_email to settings menu
		add_action( 'admin_menu', array($this, 'add_site_email' ));
	}

	//Main site email addess settigs menu
	public function add_site_email() {
		add_options_page( __('Site Email Options', 'MK_SITE_EMAIL'),  __('Site Email', 'MK_SITE_EMAIL'), 'manage_options', 'site-email', 'site_email_plugin_options' );

		function site_email_plugin_options() {
			if ( !current_user_can( 'manage_options' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
			}


			if ( isset($_POST[ 'site_email' ]) && wp_verify_nonce($_POST['site_email_nonce_field'], 'site_email_action') ) {
				// Read their posted value
				$opt_val = $_POST[ 'site_email' ];

				//this could really live in some template file....
				echo '
							<div id="setting-error-settings_updated" class="updated settings-error">
							<p><strong>Settings saved.</strong></p></div>
							<div class="wrap">
							<div id="icon-options-general" class="icon32"><br /></div><h2>Site Email Settings</h2>
							<form method="post" action="">
							<table class="form-table">
							<tr valign="top">
							<th scope="row">Site Email</th>
							<td><fieldset><legend class="screen-reader-text"><span>Site Email</span></legend>

							<label for="site_email">Site Email Address</label><br/>
							<input type="text" name="site_email" id="site_email" value="'.$opt_val.'" class="regular-text" /><br />
								<p class="description">Please enter the email adress that this site will use to send e-mails.</p>
								</fieldset></td>
								</tr>
								</table>
								'.wp_nonce_field("site_email_action", "site_email_nonce_field").'
									<p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="'.esc_attr("Save Changes").'"  /></p></form>
									</div>';



				// Save the posted value in the database
				update_option( 'site_email', $opt_val );

			} else {
				echo '<div class="wrap">
									<div id="icon-options-general" class="icon32"><br /></div><h2>Site Email Settings</h2>
									<form method="post" action="">
									<table class="form-table">
									<tr valign="top">
									<th scope="row">Site Email</th>
									<td><fieldset><legend class="screen-reader-text"><span>Site Email</span></legend>

									<label for="site_email">Site Email Address</label><br/>
									<input type="text" name="site_email" id="site_email" value="'.get_option( "site_email" ).'" class="regular-text" /><br />
								<p class="description">Please enter the email adress that this site will use to send e-mails.</p>
								</fieldset></td>
								</tr>
								</table>
								'.wp_nonce_field("site_email_action", "site_email_nonce_field").'
								<p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="'.esc_attr("Save Changes").'"  /></p></form>
										</div>';
			}
		}
	}
	

	public function wp_revisal_save($post_id, $post){
		global $current_user, $pagenow;
		get_currentuserinfo();
		

		//don't do anythig if this is not a Community Editor
		if (!in_array( "community_editor", $current_user->roles ) )
			return; 
		
		// don't do anything on autosave or auto-draft either or massupdates 
		if ( wp_is_post_autosave( $post_id ) || $post->post_status == 'auto-draft' )
			return;
		

		 // If this is just a revision, don't send the email.
		if ( wp_is_post_revision( $post_id ) )
			return;
		
		//get author info of this specific post -  IN SALVYS WORKFLOW, OFTEN TIMES THIS WILL JUST BE THE ADMIN
		$author = get_userdata($post->post_author);

		$user_community = get_user_meta( $current_user->ID, 'relatedcommunity', true ); 

		//is this a revised post? if so if will contain this meta data (parent post id)
		$_pc_liveId = get_post_meta($post_id, Revisal::POST_PARENT_META, true);
		

		//if we are editing || saving a new post - WP does not give a good way to detect this.
		#edit -  this should never occur with a community_editor... they cannot publish posts
		if($post->post_status == 'publish'){
			$post_url = get_permalink( $post_id );
			$subject = 'A post has been published';
			
			$message = "A post has been published on your website:<br /><br />";
			$message .= $post->post_title . ": <a href='".get_edit_post_link( $post_id )."'>Edit</a><br /><br />";
			$message .= "Author: " . $author->user_login . "<br /><br />"; //give users name
			$message .= "Content: " . $post->post_content . "<br /><br />";
			$this->send_mail($this->get_site_email(), $subject, $message);

		#new post not a revision/revisal
		} else if($post->post_status == 'draft' && !$_pc_liveId) {
			//echo 'new pending';
			//die();
			$post_url = get_permalink( $post_id );
			$subject = 'A new post has been created';
		
			$message = "A post has been created on your website:<br /><br />";
			$message .= $post->post_title . ": <a href='".get_edit_post_link( $post_id )."'>Edit</a><br /><br />";
			$message .= "Author: " . $author->user_login . "<br /><br />";
			$message .= "Username: " . $current_user->user_login . "<br /><br />";
			$message .= "User Email: " . $current_user->user_email . "<br /><br />";
			$message .= "User Community: " . (isset($user_community) ? $user_community : '') . "<br /><br />";
			$message .= "Post Type: " . $post->post_type . "<br /><br />";
			$message .= "Content: " . $_POST['post_content'] . "<br /><br />";
			
			
			$this->send_mail($this->get_site_email(), $subject, $message);
		#revision/revisal of new post
		} else if($post->post_status == 'pending' && $_pc_liveId){
			//echo 'new revisal';
			//die();
			$post_url = get_permalink( $post_id );
			$subject = 'A post revision has been created';
		
			$message = "A post has revision has been created on your website:<br /><br />";
			$message .= $post->post_title . ": <a href='".get_edit_post_link( $post_id )."'>Edit</a><br /><br />";
			$message .= "Author: " . $author->user_login . "<br /><br />";
			$message .= "Username: " . $current_user->user_login . "<br /><br />";
			$message .= "User Email: " . $current_user->user_email . "<br /><br />";
			$message .= "User Community: " . (isset($user_community) ? $user_community : '') . "<br /><br />";
			$message .= "Post Type: " . $post->post_type . "<br /><br />";
			$message .= "Content: " . $_POST['post_content'] . "<br /><br />";
			$this->send_mail($this->get_site_email(), $subject, $message);
		}
    }

    private function send_mail($to, $subject, $message){
    	add_filter( 'wp_mail_content_type', array($this, 'set_html_content_type' ));
		
		
		$mail = wp_mail( $to, $subject, $message );
		
		// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
		remove_filter( 'wp_mail_content_type', array($this, 'set_html_content_type' ));
    }

    public function set_html_content_type($content_type) {
			return 'text/html';
	}

	public function get_site_email(){
		$site_email = get_option('site-email');
		
		if(!$site_email){
			$site_email = self::EMAIL_TO;
		} 
		
		return $site_email;
		
	}
	
}